package com.android.settings;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.Locale;
public class RemoteFragment extends Fragment{

	//TextView remote_des2;
	ImageView mImageView;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_remote, container, false);
		//remote_des2 = (TextView)rootView.findViewById(R.id.remote_des2);
		mImageView = (ImageView)rootView.findViewById(R.id.remote_pic);

		return rootView;
	}

	/*	
	@Override
	public void onCreate(Bundle icicle) {
		// TODO Auto-generated method stub
		super.onCreate(icicle);
		addPreferencesFromResource(R.xml.fragment_remote);
		remote_des2 = (TextView)findViewById(R.id.remote_des2);
	}*/

	//Settings.System.DEVICE_NAME

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//updateDes();
	}

	private void updateDes()
	{  
		//String devicename = Settings.System.getString(mContext.getContentResolver(), Settings.System.DEVICE_NAME);
		//remote_des2.setText(getResources().getString(R.string.remote_description2, devicename));
		String url = getResources().getString(R.string.remote_url);
		if(url != null && url.length() > 0)
		{
			if(isZh())
			{
			    url=url+"?local=zh";	
			}
			try {
				Bitmap bm = EncodingHandler.createQRCode(url, 400);
				if(bm!=null)mImageView.setImageBitmap(bm);
			} catch (Exception e) {
				// TODO: handle exception
			}
			
		}
	}
	private boolean isZh() {
	        Locale locale = getResources().getConfiguration().locale;
		        String language = locale.getLanguage();
			if (language.endsWith("zh"))
				return true;
			else
				return false;
	} 
								    
}
